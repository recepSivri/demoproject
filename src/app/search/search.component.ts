import { Component, ElementRef, ViewChild } from '@angular/core';
import { Services } from '../services/service';
import { DxDataGridComponent } from 'devextreme-angular';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent {

  @ViewChild('search') private searchItem: ElementRef;
  @ViewChild('gridContainer') gridContainer: DxDataGridComponent;
  datas: any[];

  constructor(private service: Services) {
  }
  searchValue = () => {
    this.service.SerarchItem(this.searchItem.nativeElement.value).subscribe(
      (data: any) => {
        this.datas = data.results;
        this.gridContainer.instance.refresh();
      },
      err => {
      }
    );
  };
}
