import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { Services } from '../services/service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;

  email: any;
  password: any;
  emailError = false;
  passwordError = false;
  emailFormat = false;

  constructor(private formBuilder: FormBuilder, private http: HttpClient, private service: Services,
              private toastr: ToastrService, private router: Router) {
  }

  // tslint:disable-next-line:typedef
  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required]]
    });
    this.email = this.loginForm.get('email');
    this.password = this.loginForm.get('password');
  }

  onSubmit = () => {
    if (this.loginForm.invalid) {
      this.emailError = this.email.errors?.required ? this.email.errors?.required : false;
      this.passwordError = this.password.errors?.required ? this.password.errors?.required : false;
      this.emailFormat = this.email.errors?.email ? this.email.errors?.email : false;
    } else {
      this.emailError = false;
      this.passwordError = false;
      this.emailFormat = false;

      this.service.loginService().subscribe(
        data => {
          this.toastr.success('Successfully Login');
          this.router.navigate(['/search']);
        }, err => {
          this.toastr.error('Login Error');
        });

    }
  };

}
